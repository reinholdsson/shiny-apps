$(document).on("onchange", "select.chzn-select", function(evt) {
    
    // evt.target is the button that was clicked
    var el = $(evt.target);
    
    // Set the buttons text to its current value plus 1
    //el.text(parseInt(el.text()) + 1);
    
    // Raise an event to signal that the value changed
    el.trigger("onchange");
});


var incrementBinding = new Shiny.InputBinding();
//$(".chzn-select").chosen();

$.extend(incrementBinding, {
    //$(".chzn-select").chosen();
    find: function(scope) {
        return $(scope).find(".chzn-select").chosen();
    },
    subscribe: function(el, callback) {
        $(el).on("chzn-select", function(e) {
            callback();
        });
    },
    unsubscribe: function(el) {
        $(el).off(".chzn-select");
    }
});
    
Shiny.inputBindings.register(incrementBinding);